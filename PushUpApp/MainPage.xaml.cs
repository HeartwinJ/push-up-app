﻿using System;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PushUpApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int count = 0;

        public MainPage()
        {
            this.InitializeComponent();
            DisplayInformation.AutoRotationPreferences = Windows.Graphics.Display.DisplayOrientations.Portrait;
            textBox.TextAlignment = TextAlignment.Center;
            textBox.Text = Convert.ToString(count);
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            count++;
            textBox.Text = Convert.ToString(count);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            count = 0;
            textBox.Text = Convert.ToString(count);
        }
    }
}
